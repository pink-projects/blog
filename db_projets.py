liste_projets = [
    {
        "titre": "Graphe prix de stock",
        "slug": "projet-1",
        "url":"https://gitlab.com/pink-projects/bloc_1",
        "description": "Utilisation de Python et de la bibliothèque Turtle pour créer des graphiques interactifs à partir de données boursières, comme celles d'Amazon.",
        "fonctionnalites": "Chargement dynamique des données, personnalisation du graphique, visualisation interactive via Turtle",
        "prerequis": "Python 3.x, module turtle (inclus dans la plupart des installations de Python), module csv pour la lecture des fichiers CSV, fichiers CSV contenant les données à visualiser",
        "images": ""
    },
    {
        "titre": "Assistant vocal",
        "slug": "projet-2",
        "url" : "https://gitlab.com/pink-projects/assistant-vocal",
        "description": "Script en Python exploitant l'API OpenWeatherMap pour fournir des bulletins météo vocaux pour Montréal.",
        "fonctionnalites": "Récupération des données météorologiques via l'API OpenWeatherMap, synthèse vocale : Annonce des conditions météorologiques à l'utilisateur via pyttsx3",
        "prerequis": "Python 3.x, bibliothèques Python spécifiées dans le fichier requirements.txt.",
        "images" :""
    },
    {
        "titre": "Détecteur de visages",
        "slug": "projet-3",
        "url":"https://gitlab.com/pink-projects/detecteur-de-visages",
        "description": "Projets impliquant la reconnaissance faciale et la permutation de visages dans les images, utilisant des scripts Python avancés.",
        "fonctionnalites": "changement visage.py : Intervertit les visages de deux personnes sur une seule image, visages.py : Détecte les visages et les yeux sur une image",
        "prerequis": "Python 3.x, bibliothèques Python spécifiées dans le fichier requirements.txt.",
        "images" :""
    },
     {
        "titre": "Blog",
        "slug": "projet-4",
        "url":"https://gitlab.com/pink-projects/blog",
        "description": "Il s'agit de ce présent projet",
        "fonctionnalites": "",
        "prerequis": "",
        "images" :""
    },
    {
        "titre": "Plus de projets",
        "slug": "projet-x",
        "url":"https://gitlab.com/pink-projects",
        "description": "",
        "fonctionnalites":"" ,
        "prerequis":"" ,
        "images":""
    },
     {
        "titre": "Scratch- jeu intéractif",
        "slug": "projet-5",
        "url":"https://gitlab.com/pink-projects/scratch-jeu-interactif",
        "description": "Ce projet consiste en une page HTML qui intègre un jeu interactif Scratch spécifique. Ce jeu offre plusieurs scénarios qui évoluent selon les choix de l'utilisateur, rendant chaque partie unique.",
        "fonctionnalites":"""Jeu Interactif à Scénarios Multiples : Propose une expérience de jeu dynamique où les choix et les interactions, y compris les clics de souris, influencent le déroulement de l'histoire. Intégration Fullscreen : Maximise l'espace visuel disponible pour une immersion totale dans le jeu grâce à des styles CSS optimisés.""" ,
        "prerequis":"Un navigateur web moderne supportant HTML5 et JavaScript, tel que Chrome, Firefox, Safari, ou Edge." ,
        "images":""
    },
    {
        "titre": "Collection Python",
        "slug": "projet-6",
        "url":"https://gitlab.com/pink-projects/Collection_Python",
        "description": "Les scripts couvrent une large gamme de fonctionnalités, incluant des calculs mathématiques, des jeux interactifs, et des utilitaires pratiques pour la vie quotidienne. Les utilisateurs peuvent également explorer le cryptage de texte pour la sécurité des données et des fonctionnalités basées sur l'intelligence artificielle pour l'analyse avancée et la génération de contenu",
        "fonctionnalites":"" ,
        "prerequis":"""Python 3.x Aucune dépendance externe n'est nécessaire pour la majorité des scripts, sauf mention contraire dans le code source.""" ,
        "images":""
    },
    {
        "titre": "Collection React",
        "slug": "projet-7",
        "url":"https://gitlab.com/pink-projects/collection-React",
        "description": "",
        "fonctionnalites":"" ,
        "prerequis":"" ,
        "images":""
    },
    
]

# def ajouter_projet(titre, slug, url, description, fonctionnalites, prerequis, images):
#     projet = {
#         "titre": titre,
#         "slug": slug,
#         "url": url,
#         "description": description,
#         "fonctionnalites": fonctionnalites,
#         "prerequis": prerequis,
#         "images": images  # Supposons que cela soit juste un chemin ou une URL pour simplifier
#     }
#     liste_projets.append(projet)