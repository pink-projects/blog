import os
from flask import Flask,render_template,request,redirect,url_for,session,make_response,jsonify,flash

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime,timedelta
from functools import wraps
import jwt
import sys


#configuration
app = Flask(__name__)
SECRET_KEY = os.getenv('SECRET_KEY')

if not SECRET_KEY:
    sys.exit("ERROR: The SECRET_KEY environment variable is not set.")
    
app.config['SECRET_KEY'] = SECRET_KEY


DOSSIER_UPLOAD= "static/images"

#authentification

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # Vérifie d'abord la session
        if session.get('logged_in'):
            return f(*args, **kwargs)

        # Si non connecté via session, vérifie le token
        token = request.cookies.get('auth_token')
        if not token:
            return jsonify({'Alert!': 'Token is missing!'}), 401

        try:
            payload = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
            session['logged_in'] = True  # Met à jour la session après validation du token
        except jwt.ExpiredSignatureError:
            return jsonify({'Alert!': 'Token expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'Alert!': 'Invalid Token'}), 403

        return f(*args, **kwargs)

    return decorated
@app.route('/logout')
def logout():
    session.pop('logged_in', None)  # Supprime la variable de session
    response = make_response(redirect(url_for('login')))
    response.set_cookie('auth_token', '', expires=0)  # Supprime le cookie
    return response


@app.route("/log", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        # Vérifiez que le nom d'utilisateur est entré et que le mot de passe est correct
        if request.form["username"] and request.form["password"] == 'sasuke0703':
            session['logged_in'] = True
            token = jwt.encode({
                'username': request.form["username"],
                'expiration': str(datetime.utcnow() + timedelta(seconds=120))
            }, app.config['SECRET_KEY'], algorithm="HS256")

            response = make_response(redirect(url_for('admin')))
            response.set_cookie('auth_token', token, httponly=True)  # Sécurise le cookie
            return response
        else:
            # Utilisez flash pour signaler une erreur de connexion
            flash('Identifiant ou mot de passe incorrect.', 'error')
    return render_template("login.html")


#base de données
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///blog.db'
blog=SQLAlchemy(app)
migrate = Migrate(app,blog)
class Projets(blog.Model):
    id= blog.Column(blog.Integer, primary_key=True)
    titre=blog.Column(blog.String(150))
    slug = blog.Column(blog.String(150))
    url=blog.Column(blog.String(150))
    description=blog.Column(blog.Text)
    fonctionnalites=blog.Column(blog.Text)
    prerequis=blog.Column(blog.Text)
    images=blog.Column(blog.String(150))
    date= blog.Column(blog.DateTime, default=datetime.utcnow)

    def __repr__(self):
     return f"<Projet[{self.id}]: {self.titre}>"

class Competences(blog.Model):
    id= blog.Column(blog.Integer, primary_key=True)
    titre=blog.Column(blog.String(150))
    slug = blog.Column(blog.String(150))
    list=blog.Column(blog.Text)

    def __repr__(self):
     return f"<Competences[{self.id}]: {self.titre}>"



#Accueil

@app.route("/")
def accueil():
    return render_template("alternative.html")


#projets
@app.route("/projets/creer", methods=["GET","POST"])
def creer_projet():
    if request.method=="POST":
            titre=request.form["titre"]
            slug=request.form["slug"]
            url=request.form["url"]
            description=request.form["description"]
            fonctionnalites=request.form["fonctionnalites"]
            prerequis=request.form["prerequis"]
            images=request.files["images"]
            
            image_url =f'/img.jpeg'
            if images.filename != '':
                image_url = f'/{images.filename}'
                images.save(os.path.join(DOSSIER_UPLOAD,images.filename))
    
            projet= Projets(titre=titre,slug=slug,url=url,description=description,fonctionnalites=fonctionnalites,prerequis=prerequis,images=image_url)
            blog.session.add(projet)
            blog.session.commit()
            return redirect(url_for('admin'))
    return render_template("creer_projet.html")

@app.route("/projets/")
@app.route("/projets/<string:slug>")
def projets(slug=""):
    if slug:
        projet= Projets.query.filter_by(slug=slug).first_or_404()
        return render_template("projet.html",projet=projet)
    projets = Projets.query.order_by(Projets.date.desc()).all()
    return render_template("projets.html",projets=projets)



#competences
@app.route("/competences/creer", methods=["GET","POST"])
def creer_competences():
    if request.method=="POST":
            titre=request.form["titre"]
            slug=request.form["slug"]
            list=request.form["list"]
            competence=Competences(titre=titre,slug=slug,list=list)
            blog.session.add(competence)
            blog.session.commit()
            return redirect(url_for('admin'))
    return render_template("creer_competence.html")

@app.route("/competences/")
def competences():
    competences = Competences.query.all()
    return render_template("competences.html", competences=competences)

#admin
@app.route('/admin')
@token_required
def admin():
    competences = Competences.query.all()
    projets = Projets.query.order_by(Projets.date.desc()).all()
    return render_template('admin_page.html',projets=projets,competences=competences)

#presentation

@app.route("/presentation")
def presentation():
    return render_template("presentation.html")

#error
@app.errorhandler(404)
def page_404(error):
    return render_template("404.html"),404


