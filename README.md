# Blog Personnel avec Flask

## **Aperçu**
Ce projet repose sur Flask, un framework Python, pour créer un blog personnel interactif. Les fonctionnalités dynamiques sont gérées via SQLAlchemy pour les bases de données, permettant de créer, consulter et modifier des projets et des compétences à travers une interface web intuitive.

---

## **Structure du Projet**
- **`app.py`** : Script principal contenant la configuration de l'application, les modèles de données, et les routes.
- **Fichiers HTML** :
  - `blog.html`, `creer_projet.html`, `projet.html`, `creer_competence.html`, `competences.html`, `presentation.html`, `login.html`, `404.html` : Templates HTML pour les différentes pages.
- **`blog.css`** : Feuille de style pour définir l'apparence du blog.
- **`requirements.txt`** : Liste des dépendances nécessaires.
- **Scripts de démarrage** :
  - `run.bat` (Windows)
  - `run.sh` (Unix/Mac)
- **`blog.db`** : Base de données SQLite pour stocker les projets et compétences.
- **Fichiers Docker** :
  - `Dockerfile` : Instructions pour créer une image Docker.
  - `docker-compose.yml` : Configuration des services Docker.

---

## **Dépendances**
Les bibliothèques nécessaires sont dans le fichier `requirements.txt`, incluant :
- **Flask**
- **Flask_SQLAlchemy**
- **Flask_Migrate**
- Autres outils pour améliorer l'interaction utilisateur.

---

## **Installation et Lancement**

Pour lancer l'application, deux options sont disponibles :  
1. **Lancement manuel (sans Docker)**  
2. **Lancement avec Docker**  

### **Ouvrir un terminal Bash**  
Avant de commencer, ouvrez un terminal Bash en choisissant l'une des méthodes suivantes :  

- **Option 1 : Ouvrir directement le terminal Bash**  
   - Si votre système est configuré, lancez directement le terminal Bash depuis votre système d'exploitation.  

- **Option 2 : Suivre ces étapes**  
   1. Naviguez jusqu'au dossier où se trouve le projet.  
   2. Ouvrez un terminal depuis ce dossier.  
   3. Exécutez la commande suivante pour vous assurer que Bash est actif :  
      ```
      bash
      ```  

Une fois le terminal Bash ouvert, suivez les instructions spécifiques à l'option de lancement choisie.  

---

### **Option 1 : Lancement Manuel (Sans Docker)**

#### **1. Configuration de l'environnement virtuel**
Créez et activez un environnement virtuel :

- **Windows** :
  ```bash
  python -m venv env
  env\Scripts\activate
  ```

- **Unix/Mac** :
  ```bash
  python3 -m venv env
  source env/bin/activate
  ```

#### **2. Installation des dépendances**
Installez les dépendances :
```bash
pip install -r requirements.txt
```

#### **3. Configuration des variables d'environnement**
Créez un fichier `.env` avec le contenu suivant :
```
FLASK_APP=app.py
FLASK_ENV=development
SECRET_KEY=votre_secret_ici
DATABASE_URL=sqlite:///blog.db
```

Générez une clé secrète avec l'une des commandes suivantes :
- Avec `uuid` :
  ```bash
  python -c 'import uuid; print(uuid.uuid4().hex)'
  ```
- Avec `secrets` :
  ```bash
  python -c 'import secrets; print(secrets.token_urlsafe(12))'
  ```
- Avec `os` :
  ```bash
  python -c 'import os; print(os.urandom(12))'
  ```

Remplacez `votre_secret_ici` par la clé générée.

#### **4. Lancement de l'application**
- **Windows** : Exécutez `run.bat`.
- **Unix/Mac** : Exécutez `./run.sh`.

Ouvrez votre navigateur à l'adresse : [http://localhost:5000](http://localhost:5000).

---

### **Option 2 : Lancement avec Docker**

#### **1. Récupération du dépôt**
Clonez le dépôt Git :
```bash
git clone <URL-DU-DÉPÔT-GIT>
cd mon-projet
```

#### **2. Configuration des variables d'environnement**
Ajoutez les variables nécessaires dans le fichier `docker-compose.yml` :
```yaml
environment:
  FLASK_APP: app.py
  FLASK_ENV: development
  SECRET_KEY: votre_secret_ici
  DATABASE_URL: sqlite:///blog.db
```
Générez une clé secrète comme indiqué précédemment et remplacez `votre_secret_ici`.

#### **3. Construction et lancement du projet**
- Construisez l'image Docker :
  ```bash
  docker-compose build
  ```
- Lancez les conteneurs :
  ```bash
  docker-compose up
  ```

#### **4. Accéder à l'application**
Rendez-vous sur [http://localhost:5000](http://localhost:5000).

#### **5. Arrêter les conteneurs**
Pour arrêter les services, exécutez :
```bash
docker-compose down
```