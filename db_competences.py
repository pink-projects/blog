liste_competences = [
    {
        "titre": "Serveur, analyseur de protocoles:",
        "slug": "comp-1",
        "list":"DHCP, DNS, WildPackets OmniPee",
    },
    {
        "titre": "Programmation et/ou calculs:",
        "slug": "comp-2",
        "list" : "Matlab, Python, C/C++, Java, JavaScript, TypeScript, Html, CSS, Assembleur, SQL, Scratch.",
    },
      {
        "titre": "Cadriciel, plateforme logicielle/déploiement, bibliothèque:",
        "slug": "comp-3",
        "list":"Angular, NestJS, Express, NodeJS, React, Redux, NPM, Socket.IO, Turtle, MongoDb Atlas, AWS, GitLab Pages, Flask, NumPy, Matplotlib, Pandas, Seaborn",
    },
      {
        "titre": "Ebergeur/Protocoles:",
        "slug": "comp-4",
        "list":"GitHub, GitLab, HTTP, WebSockets, TCP/IP, UDP, RDP, Ethernet, IEEE 802.3.",
    },
      {
        "titre": "Éditeurs de codes, suite office, circuit intégré, cartes utilisées:",
        "slug": "comp-5",
        "list":"Vscode, Visual studio, Intellij idea, Eclipse, Word, Excel, Vivado, PowerPoint, PostgreSQL, SQLite, FPGA, PYNQ-Z2, basys3",
    },
      {
        "titre": "Environnement de travail/systèmes d’exploitation:",
        "slug": "comp-6",
        "list":"Linux, Windows.",
    },
      {
        "titre": "Infographie:",
        "slug": "comp-7",
        "list":"modèles (géométriques, graphiques), pipeline graphique programmable.",
    }
    
]