"""Premiere migration

Revision ID: 95d1ee856903
Revises: 
Create Date: 2024-05-30 21:07:40.288936

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '95d1ee856903'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('projets',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('titre', sa.String(length=150), nullable=True),
    sa.Column('slug', sa.String(length=150), nullable=True),
    sa.Column('url', sa.String(length=150), nullable=True),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('fonctionnalites', sa.Text(), nullable=True),
    sa.Column('prerequis', sa.Text(), nullable=True),
    sa.Column('images', sa.String(length=150), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('projets')
    # ### end Alembic commands ###
